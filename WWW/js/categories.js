//testowa tablica tablic

var categories = new Array(3);

/* Kategorie:
    Bajki
    Filmy
    Seriale
*/

//tworzenie trzech tablic 10-elementowych w tablicy categories

for(i = 0; i < categories.length; i++)
    categories[i] = []; //new Array(10);

//console.log(categories);

var fairytales = categories[0];
var movies = categories[1];
var tv_series = categories[2];

//console.log(fairytales, movies, tv_series);

fairytales.push("Pszczółka Maja",
                "Miś Uszatek",
                "Gumisie",
                "Tabaluga",
                "Myszka Miki i przyjaciele",
                "Klub przyjaciół Myszki Miki",
                "Chip i Dale: Brygada RR",
                "Krecik",
                "Smerfy",
                "Bajki z mchu i paproci",
                "Przygody rozbójnika Rumcajsa",
                "Wilk i Zając",
                "Przygody kota Filemona",
                "Bolek i Lolek",
                "Reksio",
                "Dziwne przygody Koziołka Matołka",
                "Porwanie Baltazaza Gąbki",
                "Przygód kilka wróbla Ćwirka",
                "Pomysłowy Dobromir",
                "Zaczarowany ołówek",
                "Król Maciuś Pierwszy",
                "Muminki",
                "Garfield i przyjaciele",
                "Bob Budowniczy",
                "Świnka Peppa",
                "Strażak Sam",
                "Nowe przygody Kubusia Puchatka",
                "Sąsiedzi");

//console.log(fairytales);

movies.push("Terminator",
            "Kevin sam w domu / Home Alone",
            "Top Gun",
            "Harry Potter i Komnata Tajemnic",
            "Barbie",
            "Kogel Mogel",
            "Ranczo Wilkowyje",
            "Taxi",
            "Szybcy i Wściekli / Fast and Furious",
            "Więzień Labiryntu / The Maze Runner",
            "Sami swoi",
            "Nie ma mocnych",
            "Kochaj albo rzuć",
            "Lalka",
            "Potop",
            "Noce i dnie",
            "Znachor",
            "Miś",
            "Seksmisja",
            "Psy",
            "Kiler",
            "U Pana Boga za piecem",
            "U Pana Boga w ogródku",
            "U Pana Boga za miedzą",
            "Ogniem i mieczem",
            "Krzyżacy",
            "Dzień świra",
            "Katyń",
            "Jutro idziemy do kina",
            "Plac Zbawiciela",
            "Nic śmiesznego",
            "Chłopaki nie płaczą",
            "Jak rozpętałem drugą wojnę światową",
            "Nad Niemnem",
            "Uprowadzenie Agaty",
            "Wesele",
            "Wszyscy jesteśmy Chrystusami",
            "Opowieści z Narnii: Książe Kaspian",
            "Igrzyska Śmierci",
            "Niezgodna",
            "Władca Pierścieni: Dwie Wieże",
            "Hobbit: Niezwykła podróż",
            "Piraci z Karaibów: Zemsta Salazara",
            "W pustyni i w puszczy");

//console.log(movies);

tv_series.push("Moda na Sukces / Bold and Beautiful",
               "M jak Miłość",
               "Zbuntowany Anioł",
               "Ranczo",
               "Nieustraszony",
               "Lucyfer / Lucifer",
               "Friends / Przyjaciele",
               "Dr House",
               "Świat według Kiepskich",
               "Tylko Miłość",
               "Ojciec Mateusz",
               "Komisarz Alex",
               "Komisarz Rex",
               "Stawka większa niż życie",
               "Czterej pancerni i pies",
               "Janosik",
               "Alternatywy 4",
               "Czterdziestolatek",
               "Chichot Losu",
               "Na dobre i na złe",
               "Złotopolscy",
               "Lońdyńczycy",
               "Barwy szczęścia",
               "Blondynka",
               "Lokatorzy",
               "Sąsiedzi",
               "13 posterunek",
               "I kto tu rządzi?",
               "Miodowe lata",
               "Pierwsza miłość",
               "Przyjaciółki",
               "Samo życie",
               "Hotel 52",
               "Adam i Ewa",
               "Rodzina zastępcza",
               "Szpilki na Giewoncie",
               "Graczykowie",
               "Daleko od noszy",
               "To nie koniec świata",
               "Malanowski i Partnerzy",
               "Bulionerzy",
               "Na Wspólnej",
               "Prawo Agaty",
               "Detektywi",
               "BrzydUla",
               "Magda M.",
               "Majka",
               "Sędzia Anna Maria Wesołowska",
               "Kasia i Tomek",
               "Faceci do wzięcia",
               "Doręczyciel",
               "Don Matteo",
               "Siostry",
               "CSI: Kryminalne zagadki Nowego Jorku",
               "CSI: Kryminalne zagadki Miami",
               "Sherlock",
               "Kości / Bones");

//console.log(tv_series);

//zmiana wielkości liter na duże dla wszystkich elementów każdej z tablic

fairytales = fairytales.map(element => element.toUpperCase());
movies = movies.map(element => element.toUpperCase());
tv_series = tv_series.map(element => element.toUpperCase());

//kopie tablic potrzebne do losowania bez powtórzeń

var fairytales_used = "";
var movies_used = "";
var tv_series_used = "";

//numer kategorii pytań, potrzebny do wskazania tablicy z której mają być losowane słówka - przyda się przy losowaniu w przypadku przegranej, gdy gracz kliknie przycisk "SPRÓBUJ JESZCZE RAZ"

var array_category_number = 0;

//zmienna z wylosowanym indeksem elementu z tablicy z danej kategorii
var random_word = "";

//zmienna wskazująca z której kategorii słówka będą losowane
var words_array = "";

//funkcja ustawiająca kategorię pytań

function setCategory(category_number) {
    if(category_number == 0)
        words_array = fairytales_used;

    else if(category_number == 1)
        words_array = movies_used;
    else
        words_array = tv_series_used;
}

//losowanie słówka z danej kategorii

function drawWord(category_number) {
    setCategory(category_number);

    if(category_number >= 0 && category_number <= 2) {
        console.log("Tablica words_array: ", words_array);

        //console.log("Długość tablicy words_array: ", words_array.length);

        random_word = Math.floor(Math.random()*words_array.length);

        //console.log("Wylosowany indeks: ", random_word);

        password = words_array[random_word];
        console.log("Wylosowane hasło: ", password);

        //console.log("zmiana: ", words_array[random_word], ' na ', words_array[words_array.length - 1]);

        var temp = words_array[random_word];
        words_array[random_word] = words_array[words_array.length - 1];
        words_array[words_array.length - 1] = temp;

        words_array.pop();

        console.log("ostatni indeks tablicy teraz: ", words_array.length);

        array_category_number = category_number;
        hidePassword();
        console.log(hidden_password);

        points = 0;
        bonus_points = 0;
    }

    gameStart();
}
