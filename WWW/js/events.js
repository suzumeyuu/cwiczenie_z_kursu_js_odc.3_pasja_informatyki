//funkcja odszyfrowująca hasło, w przypadku gdy gracz odgadnie literkę hasła (jest wywoływana dla jednego znaku)

String.prototype.setChar = function(id, char) {
    if(id > this.length-1)
        return this.toString();
    else
        return this.substr(0, id) + char + this.substr(id + 1);
}

/* Alternatywne rozwiązanie:
function setChar(id, char) {
    if(id < hidden_password.length-1)
        hidden_password = hidden_password.substr(0, id) + char + hidden_password.substr(id + 1);
} */

//zmienne dźwiękowe, wskazujące czy gracz kliknął prawidłową literkę czy też złą

var yes = new Audio("sounds/yes.wav");
var no = new Audio("sounds/no.wav");

//zmienna typu bool, do sprawdzenia czy litera występuje w haśle czy nie. Domyślnie jest ustawiona na false, czyli literka nie występuje w haśle

var match = false;

//funkcja sprawdzająca czy literka występuje w haśle, jest wywoływana dla wszystkich znaków zmiennej znakowej. Jeśli tak, to podmienia "-" na literkę i zmienia wartość zmiennej "match" na true, jeśli nie - zmienna "match" ma wartość domyślną false

function checkLetterInPassword(id) {
    match = false; //ustawianie wartości zmiennej na false (domyślna), ponieważ w przypadku gdy poprzednia literka pasowała, kolejna która nie pasuje i tak zostanie uznana za pasującą

    for(i = 0; i < password.length; i++) {
        if(letters[id] == password.charAt(i)) {
            hidden_password = hidden_password.setChar(i, letters[id]);
            match = true;
        }
    }
}

//funkcja zmieniająca wygląd klikniętego przycisku, a także "wyłączania" go

function disableButton(div, fontColor, backgroundColor) {
    div.style.color = fontColor;
    div.style.backgroundColor = backgroundColor;
    div.style.borderColor = fontColor;
    div.style.cursor = "default";
    div.setAttribute("onclick", ";");
}

//funkcja sprawdzająca czy literka występuje w haśle, jest wywoływana dla wszystkich znaków zmiennej znakowej. Jeśli tak, to podmienia "-" na literkę i zmienia kolor przycisku na zielony (przycisk przestaje być aktywny), jeśli nie - rysuje kolejne elementy szubienicy, zmienia kolor przycisku na czerwony (również przycisk przestaje być aktywny) i zmniejsza ilość skuch (nieudanych prób odgadnięcia literki przed przegraną). Ponadto w przypadku wygranej jak i przegranej zmienia wygląd ekranu gry. Działa poprzez wywołanie pomniejszych funkcji spełniających powyższe zadania

function checkLetter(id) {
    checkLetterInPassword(id);

    var div = document.getElementById("letter" + id);

    if(match != true) {
        no.play();

        disableButton(div, "#C00000", "#330000");

        if(attempts_counter < 9) {
            attempts_counter++;

            showAvailableMoves();

            document.getElementById("hanging_tree").innerHTML = '<img src="img/s' + attempts_counter + '.jpg" alt="" />';
        }

        else
            defeat(array_category_number);
    }

    else {
        yes.play();

        disableButton(div, "#00C000", "#003300");

        //drawOnBoard();

        //TODO: można pomyśleć nad funkcją sprawdzającą znak po znaku czy zmienne "password" oraz "hidden_password" są takie same, coś w stylu funkcji sprawdzającej czy znak występuje w haśle
        if(password === hidden_password)
            victory(array_category_number);
    }

    drawOnBoard();
}

//TODO: funckcje defeat() oraz victory(): Usunąć przycisk "Poddaj się" w przypadku gry gracz wygrał, przegrał lub się poddał, ma być tylko przycisk powrotu do ekranu głównego lub/oraz do wyboru kategorii pytań

//funkcja obsługująca przegraną gracza

function defeat(category_number) {
    document.title = "Wisielec - Przegrana!";

    no.play();

    document.getElementById("available_moves").innerHTML = '';

    var right_answer = "";

    for(i = 0; i < password.length; i++) {
        if(hidden_password.charAt(i) == password.charAt(i))
            right_answer += password.charAt(i);
        else
            right_answer += '<span class="red">' + password.charAt(i) + '</span>';
    }

    hidden_password = right_answer;

    drawOnBoard();

    document.getElementById("hanging_tree").innerHTML = '<img src="img/s9.jpg" alt="" />';

    document.getElementById("alphabet").innerHTML = '<p>Przegrana! Prawidłowe hasło: "' + password + '"</p>';

    if(words_array.length <= 0)
        outOfWords(category_number);

    else {
        document.getElementById("alphabet").innerHTML += '<p><span id="defeat" onclick="drawWord(' + category_number + ')">JESZCZE RAZ?</span></p>';
        document.getElementById("nav_buttons").innerHTML = '<span id="return_button" onclick="location.reload()">Powrót do strony głównej</span>';
    }
}

//funkcja obsługująca zwycięstwo gracza

function victory(category_number) {
    document.title = "Wisielec - Zwycięstwo!";

    yes.play();

    document.getElementById("available_moves").innerHTML = '';

    drawOnBoard();

    document.getElementById("alphabet").innerHTML = '<p>Tak jest! Podano prawidłowe hasło: "' + password + '"</p><p>Ilość skuch: ' + attempts_counter + '</p>';

    points = 100;

    bonus_points += (counter_moves_left - attempts_counter) * 10;

    if(counter_moves_left - attempts_counter == 9)
        bonus_points += 100;

    total_points += points + bonus_points;

    drawPoints();

    if(words_array.length <= 0)
        outOfWords(category_number);

    else {
        document.getElementById("alphabet").innerHTML += '<p><span id="victory" onclick="drawWord(' + category_number + ')">JESZCZE RAZ?</span></p>';
        document.getElementById("nav_buttons").innerHTML = '<span id="return_button" onclick="location.reload()">Powrót do strony głównej</span>';
    }
}

function outOfWords(category_number) {
    document.title = "Wisielec - Koniec słówek!";

    document.getElementById("available_moves").innerHTML = '';

    //drawOnBoard();

    var category_name = "";

    if(category_number == 0)
        category_name = "BAJKI";
    else if(category_number == 1)
        category_name = "FILMY";
    else
        category_name = "SERIALE";

    document.getElementById("alphabet").innerHTML += '<p>Koniec słówek! Odgadłeś wszystkie słówka z kategorii ' + category_name + '</p>';

    document.getElementById("alphabet").innerHTML += '<p>Twój wynik: ' + total_points + '</p>';

    //document.getElementById("alphabet").innerHTML += '<p><span id="return_button" onclick="chooseCategory()">POWRÓT DO WYBORU KATEGORII</span></p>';

    document.getElementById("nav_buttons").innerHTML = '<span id="return_temp" onclick="location.reload()">Powrót do strony głównej</span><span id="return_temp2" onclick="chooseCategory()">POWRÓT DO WYBORU KATEGORII</span>';
    //document.getElementById("nav_buttons").innerHTML = '<span id="return_button" onclick="initGame()">Powrót do strony głównej</span>';
}
