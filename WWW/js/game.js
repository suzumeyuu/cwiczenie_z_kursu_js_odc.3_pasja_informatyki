//tablica zawierająca litery (za jakiś czas również i cyfry)

const letters = [];

letters.push("A", "Ą", "B", "C", "Ć", "D", "E", /*"0", "5",*/
             "Ę", "F", "G", "H", "I", "J", "K", /*"1", "6",*/
             "L", "Ł", "M", "N", "Ń", "O", "Ó", /*"2", "7",*/
             "P", "Q", "R", "S", "Ś", "T", "U", /*"3", "8",*/
             "V", "W", "X", "Y", "Z", "Ż", "Ź" /*,"4", "9"*/);


//licznik nieudanych prób odgadnięcia hasła

var attempts_counter = 0;

//licznik dostępnych skuch do wykorzystania

var counter_moves_left = 9;

//funkcja wypisująca ile skuch pozostało graczowi do wykorzystania, nim gra zakończy się przegraną

function showAvailableMoves()
{
    document.getElementById("available_moves").innerHTML = '<span>Pozostało: ';

    if(counter_moves_left - attempts_counter > 6)
        document.getElementById("available_moves").innerHTML += '<span class="green">' + (counter_moves_left - attempts_counter)  + '</span>';

    else if((counter_moves_left - attempts_counter <= 6) && (counter_moves_left - attempts_counter > 3))
        document.getElementById("available_moves").innerHTML += '<span class="yellow">' + (counter_moves_left - attempts_counter) + '</span>';

    else
        document.getElementById("available_moves").innerHTML += '<span class="red">' + (counter_moves_left - attempts_counter)  + '</span>';

    document.getElementById("available_moves").innerHTML += ' skuch</span>';
}

//funkcja wypisująca ilość punktów na ekranie

function drawPoints() {
    document.getElementById("points").innerHTML = "Punkty: " + total_points;

    if(points > 0)
        document.getElementById("points").innerHTML += '<br><span class="green"> +' + (points + ((counter_moves_left - attempts_counter) * 10)) + ' punktów!</span>';
    if((counter_moves_left - attempts_counter) == 9 && points > 0)
        document.getElementById("points").innerHTML += '<br><span class="yellow"> BONUS: +' + (bonus_points - ((counter_moves_left - attempts_counter) * 10)) + ' punktów!</span>';
}

//licznik punktów za dany poziom (czyli pojedyncze słówko do odgadnięcia)
var points = 0;

//bonusowe punkty za niewykorzystane skuchy + punkty za niezużycie żadnej skuchy
var bonus_points = 0;

//całkowita ilość punktów zdobyta do tej pory
var total_points = 0;

function drawOnBoard() {
    document.getElementById("board").innerHTML = '<span id="password">' + hidden_password + '</span>';
}

function drawHangingTree() {
    document.getElementById("box").innerHTML += '<div id="hanging_tree"><img src="img/s0.jpg" alt="" /></div>';
}

//funkcja generująca przyciski z literami (za jakiś czas również cyfry)

function generateLetterButtons() {
    var generatedCode = "";

    for(i = 0; i < letters.length; i++) {

        if(i % 7 == 0) /*(i % 9 == 0)*/
            generatedCode += "<div style=\"clear: both;\"></div>";
        generatedCode += "<div id=\"letter" + i + "\" class=\"letters\" onclick=\"checkLetter(" + i + ")\">" + letters[i] + "</div>";
    }

    document.getElementById("box").innerHTML += '<div id="alphabet">' + generatedCode + '</div>';
}


//funkcja rysująca ekran gry

function gameStart() {
    document.title = "Wisielec";

    attempts_counter = 0;

    document.getElementById("box").innerHTML = '<div id="board"></div>';

    document.getElementById("box").innerHTML += '<div id="available_moves"></div>';

    document.getElementById("box").innerHTML += '<div id="points"></div>';

    showAvailableMoves();

    drawPoints();

    drawOnBoard();

    drawHangingTree();

    generateLetterButtons();

    document.getElementById("box").innerHTML += '<div style="clear: both"></div>' + '<div id="nav_buttons"></div>';

    document.getElementById("nav_buttons").innerHTML += '<span id="return_temp" onclick="location.reload()">Powrót do strony głównej</span>' + '<span id="surrender_temp" onclick="defeat(' + array_category_number + ')">Poddaj się</span>';
}
