//zmienne z kodem HTML, który powtarza się więcej niż raz (stopka to wyjątek od reguły)

//nagłówek strony

var title = '<div id="title">W<span class="red">I</span>S<span class="red">I</span>ELEC</div>';

//stopka strony
//TODO: Edytować zawartość nagłówka, gdyż dużo więcej pracy włożyłam w rozbudowę tej gierki niżeli było w odcinku kursu

var footer = '<div id="footer"><hr><p>Kontakt: <a href="mailto:mwawryniuk01@gmail.com" target="_blank" title="Masz jakieś pytania lub uwagi? Napisz do mnie!">mwawryniuk01@gmail.com</a>. Strona zrobiona w ramach kursu JavaScript z kanału Pasja Informatyki <a href="https://www.youtube.com/watch?v=9FVtiJHFCSU" target="_blank" title="Kliknij, aby przejść do strony z odcinkiem">(Kurs JS odc. 3)</a></p></div>';

//funkcja rysująca ekran wyboru kategorii pytań oraz wypełniająca tablice kategorii słówek słówkami z oryginalnych tablic za każdym razem, gdy ktoś przejdzie do ekranu wyboru kategorii słówek. Dzięki temu nie ma możliwości, aby tablice do losowania słówek były puste pomimo iż wcześniej gracz wszystkie słówka próbował odgadnąć

function chooseCategory() {
    document.title = "Wisielec - Wybierz kategorię";

    var categories = '<p>Wybierz kategorię: </p><div id="categories"><span class="category_buttons" onclick="drawWord(0)">Bajki</span><span class="category_buttons"onclick="drawWord(1)">Filmy</span><span class="category_buttons" onclick="drawWord(2)">Seriale</span><span id="return_button" onclick="initGame()">Powrót do strony głównej</span></div>';

    document.getElementById("box").innerHTML = title + categories;

    fairytales_used = Array.from(fairytales);
    movies_used = Array.from(movies);
    tv_series_used = Array.from(tv_series);

    console.log(fairytales_used, movies_used, tv_series_used);
}

//funkcja rysująca ekran startowy gry

function initGame() {
    var image = '<div id="hanging_man_img"><img src="img/s9.jpg" alt="" /></div>';
    var start_button = '<span class="buttons" onclick="chooseCategory()">START!</span>';

    document.getElementById("container").innerHTML = '<div id="box"></div>' + footer;

    document.getElementById("box").innerHTML = title + image + start_button;
}

//funkcja wywołująca initGame(), wczytywana podczas wejścia na stronę/przeładowania strony

function start() {
    initGame();
}


//wczytuje funkcję start() przy starcie strony

window.onload = start;
